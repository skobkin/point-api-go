package point

// Page represents the basic feed API response which contains array of posts and has_next flag
type Page struct {
	HasNext bool       `json:"has_next"`
	Posts   []MetaPost `json:"posts"`
	Error   string     `json:"error"`
}
