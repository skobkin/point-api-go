package point

// User represents the user object in API response
type User struct {
	Id    int    `json:"id"`
	Login string `json:"login"`
}
