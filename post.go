package point

// Post represents the basic post data
type Post struct {
	Id            string   `json:"id"`
	Author        User     `json:"author"`
	Tags          []string `json:"tags"`
	Text          string   `json:"text"`
	Files         []string `json:"files"`
	CommentsCount int      `json:"comments_count"`
	Created       string   `json:"created"`
	Pinned        bool     `json:"pinned"`
	Private       bool     `json:"private"`
}
