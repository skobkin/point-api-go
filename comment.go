package point

// Post represents the post comment data
type Comment struct {
	PostId   string   `json:"post_id"`
	Number   int      `json:"id"`
	ToNumber int      `json:"to_comment_id"`
	IsRec    bool     `json:"is_rec"`
	Author   User     `json:"author"`
	Text     string   `json:"text"`
	Files    []string `json:"files"`
	Created  string   `json:"created"`
}
