package point

// Represents the WebSocket post comment data
type WSComment struct {
	/*
	{
	  "a": "comment",
	  "files": null,
	  "to_text": "test",
	  "author": "skobkin-ru",
	  "text": "test",
	  "comment_id": 2,
	  "to_comment_id": "1",
	  "html": "<p>test</p>",
	  "post_id": "ojpot"
	}
	 */

	A           string   `json:"a"`
	Files       []string `json:"files"`
	ToText      string   `json: "to_text"`
	Author      string     `json:"author"`
	Text        string   `json:"text"`
	CommentId   int     `json:"comment_id"`
	ToCommentId string  `json:"to_comment_id"`
	PostId      string   `json:"post_id"`
}
