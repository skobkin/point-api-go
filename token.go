package point

// Token represents the login API method response and contains important login data
type Token struct {
	AuthToken string `json:"token"`
	CsRfToken string `json:"csrf_token"`
	Error     string `json:"error"`
}
