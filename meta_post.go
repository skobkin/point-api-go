package point

// MetaPost represents the meta-post object in API response. It contains the post object and some user dependent data
type MetaPost struct {
	Bookmarked  bool `json:"bookmarked"`
	Uid         int  `json:"uid"`
	Subscribed  bool `json:"subscribed"`
	Editable    bool `json:"editable"`
	Recommended bool `json:"recommended"`
	Post        Post `json:"post"`
}
