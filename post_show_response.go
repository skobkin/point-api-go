package point

// PostShowResponse represents /api/post response including post data and comments
type PostShowResponse struct {
	Post     Post      `json:"post"`
	Comments []Comment `json:"comments"`
	Error    string    `json:"error"`
}
